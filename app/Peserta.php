<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peserta extends Model
{
    protected $fillable = [
        'nomor',
        'nama',
        'tanggal_grace',
        'program'
    ];
}
