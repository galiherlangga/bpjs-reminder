<?php

namespace App\Exports;

use App\Peserta;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PesertaExport implements FromView
{
    public function view(): View
    {
        $peserta = Peserta::where('tanggal_grace', date('Y-m-d'))->get();
        // $peserta = Peserta::all();
        return view('export_peserta', compact('peserta'));

    }
}
