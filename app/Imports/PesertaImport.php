<?php

namespace App\Imports;

use App\Peserta;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class PesertaImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        error_log('============================');
        error_log('begin import');
        // $unix_tanggal_lahir = ($row[3] - 25569) * 86400;
        $unix_tanggal_grace = ($row[2] - 25569) * 86400;
        // $tanggal_lahir = gmdate('Y-m-d',$unix_tanggal_lahir);
        $tanggal_grace = gmdate('Y-m-d',$unix_tanggal_grace);
        // error_log($tanggal_lahir);
        // $tanggal_lahir = date('Y-m-d', $tanggal_lahir);
        return new Peserta([
            'nomor'=>$row[0],
            'nama'=>$row[1],
            'tanggal_grace'=>$tanggal_grace,
            'program'=>$row[3]
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }

}
