<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SiteCounter;

class SiteCounterController extends Controller
{
    public function counter() {
        $counter = SiteCounter::where('month', date('m'))->where('year', date('Y'))->first();
        if ($counter){
            $counter->counter += 1;
            $counter->save();
        }else{
            $counter = new SiteCounter();
            $counter->counter = 1;
            $counter->month = date('m');
            $counter->year = date('Y');
            $counter->save();
        }
        return redirect()->to('https://www.bpjsketenagakerjaan.go.id/bpu');
    }

    public function getCounter(Request $request) {
        $year = isset($request->year)?$request->year:date('Y');
        $result = [];
        for ($i=1;$i<=12;$i++){
            $month = $i<10?'0'.$i:$i;
            $counter = SiteCounter::where('year', $year)
                    ->where('month',$month)->first();
            array_push($result, isset($counter->counter)?$counter->counter:0);
        }
        return $result;
        
    }
}
