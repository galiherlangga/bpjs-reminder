<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Imports\PesertaImport;
use App\Exports\PesertaExport;
use App\Peserta;
use Maatwebsite\Excel\Facades\Excel;

class PesertaController extends Controller
{
    public function index(Request $request)
    {
        
        if (isset($request->tanggal)){
            $tanggal = explode('-', $request->tanggal);
            $awal = date('Y-m-d',strtotime(str_replace(' ', '', $tanggal[0])));
            $akhir = date('Y-m-d',strtotime(str_replace(' ', '', $tanggal[1])));
            $peserta = Peserta::whereBetween('tanggal_grace', [$awal, $akhir])->get();
        }else{
            $peserta = Peserta::all();
        }
        $data = [
            'isPeserta'=>'active',
            'peserta'=>$peserta
        ];
        return view('peserta.index', $data);
    }

    public function show(Request $request)
    {
        $filename = 'peserta_'.date('d-m-Y').'.xlsx';
        return Excel::download(new PesertaExport(), $filename);
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file'=>'required|mimes:csv,xls,xlsx'
        ]);
        $file = $request->file('file');
        $nama_file = rand().$file->getClientOriginalName();
        $file->move(storage_path('/excel/peserta'),$nama_file);
        Excel::import(new PesertaImport, storage_path('/excel/peserta/'.$nama_file));
        

        return redirect()->back()->with(['success'=>'Data Peserta berhasil disimpan']);
    }

    public function export(){
        $filename = 'peserta_'.date('d-m-Y').'.xlsx';
        return Excel::download(new PesertaExport(), $filename);
    }

    public function getMonthly(Request $request){
        $year = isset($request->year)?$request->year:date('Y');
        $result = [];
        // $peserta = Peserta::where('tanggal_grace','LIKE','2021-01-%')->get();
        // dd($peserta);
        for($i=1;$i<=12;$i++){
            $month = strlen((string)$i)==1?'0'.$i:$i;
            $peserta = Peserta::where('tanggal_grace','LIKE', $year.'-'.$month.'-%')->count();
            array_push($result,$peserta);
        }
        return $result;
    }
}
