<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peserta;
use App\Berkas;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $monthList = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $filterDate = isset($request->date) ? $request->date : date('Y-m-d');
        $peserta = Peserta::where('tanggal_grace', $filterDate)->get();

        $berkas = Berkas::where('status', 'Menunggu Diproses');

        $filterMonth = isset($request->month) ? array_search($request->month, $monthList) + 1 : '';
        if ($filterMonth != '' && strlen($filterMonth) < 2)
            $filterMonth = '0' . $filterMonth;
        $filterYear = isset($request->year) ? $request->year : date('Y');
        $filterBerkas = $filterYear . '-' . $filterMonth . '%';
        // dd($filterBerkas);
        $berkas = $berkas->where('created_at', 'LIKE', $filterBerkas);

        $date = date('F Y');
        $data = [
            'isDashboard' => 'active',
            'peserta' => $peserta,
            'date' => $date,
            'month' => $monthList,
            'filterDate' => $filterDate,
            'berkas' => $berkas->get(),
            'filterYear' => $filterYear,
            'filterMonth' => isset($request->month) ? $request->month : '',
        ];
        return view('dashboard.index', $data);
    }
}
