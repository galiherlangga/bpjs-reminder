<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Berkas;
use Exception;

class BerkasController extends Controller
{
    public function index(Request $request)
    {
        $data = [
            'isBerkas' => 'active',
            'berkas' => Berkas::all()
        ];
        return view('berkas.index', $data);
    }

    public function upload(Request $request)
    {
        $rules = [
            'nama' => 'required|string|max:255',
            'no_hp' => 'required|string|max:15',
            'jenis_berkas' => 'required|string|max:100',
            'file' => 'required|file'
        ];
        $errorMessage = [
            'nama.required' => 'Nama wajib diisi',
            'no_hp.required' => 'Nomor HP wajib diisi',
            'jenis_berkas.required' => 'Jenis berkas wajib dipilih',
            'file.required' => 'Berkas wajib diisi'
        ];
        $request->validate($rules, $errorMessage);

        $filename = str_replace('=', '', base64_encode(time())) . '.' . $request->file('file')->extension();
        $upload_result = $request->file('file')->move(storage_path('app/public/berkas'), $filename);
        if ($upload_result) {
            if (isset($request->id)) {
                $berkas = Berkas::find($request->id);
                unlink(storage_path('app/public/berkas/' . $berkas->file));
            } else {
                $berkas = new Berkas();
            }
            $berkas->nama = $request->nama;
            $berkas->no_hp = $request->no_hp;
            $berkas->jenis_berkas = $request->jenis_berkas;
            $berkas->file = $filename;
            $berkas->keterangan = $request->keterangan ?? null;
            $berkas->save();
        }
        return redirect()->route('berkas.index')->with('success', "Data berhasil disimpan");
    }

    public function deletefile(Request $request)
    {
        $rules = [
            'id' => 'required'
        ];
        $errorMessage = [
            'id.required' => 'Berkas tidak ditemukan'
        ];
        $request->validate($rules, $errorMessage);

        $berkas = Berkas::find($request->id);
        unlink(storage_path('app/public/berkas/' . $berkas->file));
        $berkas->delete();
        return redirect()->route('berkas.index');
    }

    public function permohonan(Request $request)
    {
        if ($request->isMethod('POST')) {
            $rules = [
                'nama' => 'required|string|max:255',
                'no_hp' => 'required|string|max:15',
                'jenis_berkas' => 'required|string|max:100',
                'file' => 'required|file'
            ];
            $errorMessage = [
                'nama.required' => 'Nama wajib diisi',
                'no_hp.required' => 'Nomor HP wajib diisi',
                'jenis_berkas.required' => 'Jenis berkas wajib dipilih',
                'file.required' => 'Berkas wajib diisi'
            ];
            $request->validate($rules, $errorMessage);

            $berkas = new Berkas();
            $berkas->nama = $request->nama;
            $berkas->no_hp = $request->no_hp;
            $berkas->jenis_berkas = $request->jenis_berkas;
            $berkas->keterangan = $request->keterangan ?? null;

            $filename = str_replace('=', '', base64_encode(time())) . '.' . $request->file('file')->extension();
            $upload_result = $request->file('file')->move(storage_path('app/public/berkas'), $filename);
            if ($upload_result) {
                $berkas->file = $filename;
            }
            $berkas->save();
            return redirect()->route('berkas.permohonan')->with('success', "Data berhasil disimpan");
        }
        return view('berkas.permohonan');
    }

    public function edit(Request $request, $id)
    {
        $berkas = Berkas::find($id);
        $data = [
            "status" => 200,
            'berkas' => $berkas
        ];
        return $data;
    }

    public function update(Request $request, $id)
    {
        // try {
        $rules = [
            'nama' => 'required|string|max:255',
            'no_hp' => 'required|string|max:15',
            'jenis_berkas' => 'required|string|max:100',
        ];
        $errorMessage = [
            'nama.required' => 'Nama wajib diisi',
            'no_hp.required' => 'Nomor HP wajib diisi',
            'jenis_berkas.required' => 'Jenis berkas wajib dipilih',
        ];
        $request->validate($rules, $errorMessage);
        $berkas = Berkas::find($id);
        $berkas->nama = $request->nama;
        $berkas->no_hp = $request->no_hp;
        $berkas->jenis_berkas = $request->jenis_berkas;
        $berkas->keterangan = $request->keterangan ?? null;

        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $filename = str_replace('=', '', base64_encode(time())) . '.' . $request->file('file')->extension();
            $upload_result = $request->file('file')->move(storage_path('app/public/berkas'), $filename);
            if ($upload_result) {
                unlink(storage_path('app/public/berkas/' . $berkas->file));
                $berkas->file = $filename;
            }
        }
        $berkas->save();
        return redirect()->route('berkas.index')->with('success', 'Update berkas berhasil');
        // } catch (Exception $e) {
        // return redirect()->route('berkas.index')->with('error', 'Update berkas gagal');
        // }
    }

    public function getBerkas(Request $request)
    {
        $year = isset($request->year) ? $request->year : date('Y');
        $result = [];
        for ($i = 1; $i <= 12; $i++) {
            $month = strlen((string)$i) == 1 ? '0' . $i : $i;
            $berkas = Berkas::where('created_at', 'LIKE', $year . '-' . $month . '-%')->count();
            array_push($result, $berkas);
        }
        return $result;
    }

    public function changeStatus(Request $request, $id)
    {
        $berkas = Berkas::find($id);
        if ($berkas->status == 'Menunggu Diproses') {
            $berkas->status = "Diproses";
            $message = 'Berkas telah diproses';
        } else {
            $berkas->status = "Menunggu Diproses";
            $message = 'Berkas menunggu diproses';
        }
        $berkas->save();
        return redirect()->back()->with('success', $message);
    }
}
