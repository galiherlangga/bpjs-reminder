<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteCounter extends Model
{
    protected $fillables = [
        'counter',
        'month',
        'year',
    ];
}
