<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class Berkas extends Model
{
    protected $fillables = [
        'nama',
        'no_hp',
        'jenis_berkas',
        'file',
        'keterangan'
    ];

    public function dayCount($date){
        // $date = date('Y-m-d', strtotime($date));
        $date1 = date_create($date);
        $date2 = date_create('now');
        $interval = date_diff($date1, $date2);
        return($interval->days);
    }
}
