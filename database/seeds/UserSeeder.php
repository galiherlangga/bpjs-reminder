<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'admin';
        $user->username = 'admin';
        $user->email = 'admin@sipijat.com';
        $user->email_verified_at = date("Y-m-d H:i:s");
        $user->password = bcrypt('12345678');
        $user->save();
    }
}
