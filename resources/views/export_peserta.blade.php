<table style="width: 100%; table-layout: fixed;">
    <thead>
        <tr>
            <th style="font-family :Bookman Old Style;"><b>Nomor</b></th>
            <th style="font-family :Bookman Old Style;"><b>Nama</b></th>
            <th style="font-family :Bookman Old Style;"><b>Tanggal Grace</b></th>
            <th style="font-family :Bookman Old Style;"><b>Program</b></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($peserta as $key=>$item)
            <tr>
                <td style="font-family :Bookman Old Style;">{{$item->nomor}}</td>
                <td style="font-family :Bookman Old Style;">{{$item->nama}}</td>
                <td style="font-family :Bookman Old Style;">{{date('d-m-Y', strtotime($item->tanggal_grace))}}</td>
                <td style="font-family :Bookman Old Style;">{{$item->program}}</td>
            </tr>
        @endforeach
    </tbody>
</table>