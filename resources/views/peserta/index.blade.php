@extends('base')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Peserta</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item active">Peserta</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Daftar Peserta</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 mb-3">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#filter-modal">
                            <i class="fa fa-filter"></i>
                        </button>
                    </div>
                </div>
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nomor</th>
                            <th>Nama</th>
                            <th>Tanggal Grace</th>
                            <th>Program</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($peserta as $key=>$item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->nomor}}</td>
                            <td>{{$item->nama}}</td>
                            <td>{{$item->tanggal_grace}}</td>
                            <td>{{$item->program}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer ">
                <a href="#" class="btn btn-primary" data-toggle="modal" data-target='#import-modal'><i
                        class="fa fa-file-import"></i> Import File</a>
                <a href="{{ route('peserta.exportfile') }}" class="btn btn-primary float-right"><i
                        class="fa fa-file-export"></i> Export File</a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </div>


</section>
<!-- /.content -->
@endsection

@section('modal')
<div class="modal fade" id="import-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Import Data Peserta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('peserta.import') }}" id='form-import' method="POST"
                    enctype="multipart/form-data">@csrf
                    <div class="form-group">
                        <label for="file">File Peserta</label>
                        <input type="file" name="file" id="file-peserta" class="form-control"
                            accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                        <input type="hidden" name="return" value="peserta.index">
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" form="form-import" class="btn btn-primary">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="filter-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Filter Data Peserta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" id='form-filter' method="GET" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Date range:</label>

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="far fa-calendar-alt"></i>
                                </span>
                            </div>
                            <input type="text" name='tanggal' class="form-control float-right" id="range-filter">
                        </div>
                        <!-- /.input group -->
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" form="form-filter" class="btn btn-primary">Filter</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('script')
<script>
    $('#range-filter').daterangepicker()
</script>
@endsection