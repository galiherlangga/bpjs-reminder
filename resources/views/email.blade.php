<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email</title>

    <style>
        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 620px) {
          table[class=body] h1 {
            font-size: 28px !important;
            margin-bottom: 10px !important; 
          }
          table[class=body] p,
          table[class=body] ul,
          table[class=body] ol,
          table[class=body] td,
          table[class=body] span,
          table[class=body] a {
            font-size: 16px !important; 
          }
          table[class=body] .wrapper,
          table[class=body] .article {
            padding: 10px !important; 
          }
          table[class=body] .content {
            padding: 0 !important; 
          }
          table[class=body] .container {
            padding: 0 !important;
            width: 100% !important; 
          }
          table[class=body] .main {
            border-left-width: 0 !important;
            border-radius: 0 !important;
            border-right-width: 0 !important; 
          }
          table[class=body] .btn table {
            width: 100% !important; 
          }
          table[class=body] .btn a {
            width: 100% !important; 
          }
          table[class=body] .img-responsive {
            height: auto !important;
            max-width: 100% !important;
            width: auto !important; 
          }
        }
  
        /* -------------------------------------
            PRESERVE THESE STYLES IN THE HEAD
        ------------------------------------- */
        @media all {
          .ExternalClass {
            width: 100%; 
          }
          .ExternalClass,
          .ExternalClass p,
          .ExternalClass span,
          .ExternalClass font,
          .ExternalClass td,
          .ExternalClass div {
            line-height: 100%; 
          }
          .apple-link a {
            color: inherit !important;
            font-family: inherit !important;
            font-size: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
            text-decoration: none !important; 
          }
          #MessageViewBody a {
            color: inherit;
            text-decoration: none;
            font-size: inherit;
            font-family: inherit;
            font-weight: inherit;
            line-height: inherit;
          }
        }
  
      </style>

</head>

<body style="background-color: #f6f6f6; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body" style="background-color: #f6f6f6; width: 100%;">
        <tr>
            <td>&nbsp;</td>
            <td class="container" style="display: block; margin: 0 auto !important; max-width: 580px; padding: 10px; width: 580px;">
                <div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px; ">

                    <!-- START CENTERED WHITE CONTAINER -->
                    <table role="presentation" class="main" style="background: #ec3b3b; border-radius: 5px 5px 0 0; width: 100%; box-shadow: 5px 10px 20px rgba(0, 0, 0, 0.1);">
                        <tr>
                            <td>
                                {{-- <img src="{{ $message->embed(asset('img/paket-img.svg')) }}" style="display: block; margin: 0 auto; padding-top: 20px; width: 50%;"> --}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h1 style="text-align: center; color: #ffffff;">REMINDER</h1>
                            </td>
                        </tr>
                    </table>
                    <table role="presentation" class="main" style="background: #ffffff; border-radius: 0 0 5px 5px; width: 100%; box-shadow: 5px 10px 20px rgba(0, 0, 0, 0.1);">

                        <!-- START MAIN CONTENT AREA -->
                        <tr>
                            <td class="wrapper" style="box-sizing: border-box; padding: 20px 80px 20px 80px;">
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <p style="color: #363636; text-align: center; font-size: 12pt;">Halo, silahkan klik link berikut untuk mendapatkan daftar grace</p>
                                            
                                            <a href="javascipt:void(0)"><h1 style="color: #363636; text-align: center;">DAFTAR GRACE</h1></a>
                                            
                                            {{-- <p style="color: #363636; text-align: center; font-size: 12pt;">Paket dapat diambil pada pos yang sudah ditentukan.</p> --}}
                                            <br>
                                            <p style="color: #363636; text-align: center; font-size: 12pt;">Terima kasih,</p>

                                            <br>
                                            {{-- <p style="color: #363636; text-align: center; font-size: 12pt; font-style: italic;">- {{$name}} -</p> --}}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- END MAIN CONTENT AREA -->
                    </table>
                    <!-- END CENTERED WHITE CONTAINER -->

                </div>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
</body>

</html>