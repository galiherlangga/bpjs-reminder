@extends('base')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Berkas</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item active">Berkas</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Daftar Berkas</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 mb-3">
                        <button class="btn btn-primary" id='add-berkas-btn'>
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <p>{{ $message }}</p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <p>{{ $message }}</p>
                </div>
                @endif
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>No. Hp</th>
                            <th>Tanggal</th>
                            <th>Jenis Berkas</th>
                            <th>Status</th>
                            <th width='18%'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($berkas as $key=>$item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->nama}}</td>
                            <td>{{$item->no_hp}}</td>
                            <td>{{date('d-m-Y', strtotime($item->created_at))}}</td>
                            <td>{{$item->jenis_berkas}}</td>
                            <td>{{$item->status}}</td>
                            <td>
                                <a href="{{ asset('storage/berkas/'.$item->file) }}" target='_blank'
                                    class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                {{-- <button class="btn btn-primary edit" data-id='{{$item->id}}'><i
                                        class="fa fa-edit"></i></button> --}}
                                <button class="btn btn-primary delete" data-id='{{$item->id}}'><i
                                        class="fa fa-trash"></i></button>
                                <a href="{{ route('berkas.status', ['id'=>$item->id]) }}" class="btn btn-primary"
                                    title="ganti status"><i class="fa fa-sync"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>


</section>
<!-- /.content -->
@endsection

@section('modal')
<div class="modal fade" id="add-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id='add-modal-title'>Tambah Berkas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('berkas.upload') }}" id='form-berkas' method="POST"
                    enctype="multipart/form-data">@csrf
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" id='nama' class="form-control" name='nama' placeholder="Nama" required>
                    </div>
                    <div class="form-group">
                        <label for="no_hp">Nomor HP</label>
                        <input type="text" id='no-hp' class="form-control" name='no_hp' placeholder="Nomor HP" required>
                    </div>
                    <div class="form-group">
                        <label for="jenis_berkas">Jenis Berkas</label>
                        <select name="jenis_berkas" id="jenis-berkas" class="form-control" required>
                            <option value="">-- Pilih Jenis Berkas --</option>
                            <option value="BERKAS PENDAFTARAN PERUSAHAAN">BERKAS PENDAFTARAN PERUSAHAAN</option>
                            <option value="PERUBAHAN DATA TENAGA KERJA">PERUBAHAN DATA TENAGA KERJA</option>
                            <option value="AMALGAMASI (PENGGABUNGAN SALDO)">AMALGAMASI (PENGGABUNGAN SALDO)</option>
                            <option value="BERKAS PENUTUPAN PERUSAHAAN">BERKAS PENUTUPAN PERUSAHAAN</option>
                            <option value="LAIN-LAIN">LAIN-LAIN</option>
                        </select>
                    </div>
                    <div class="form-group" id='keterangan-row'>
                        <label for="keterangan">Keterangan</label>
                        <textarea name="keterangan" id="keterangan" cols="30" rows="4" class="form-control"
                            placeholder="Keterangan"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="file">File Peserta</label>
                        <input type="file" name="file" id="file-peserta" class="form-control"
                            accept="application/pdf, image/jpeg" required>
                        <input type="hidden" name="return" value="peserta.index">
                        <input type="hidden" name="id" id='id-berkas'>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" form="form-berkas" class="btn btn-primary">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="delete-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id='delete-modal-title'>Hapus Berkas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('berkas.deletefile') }}" id='form-hapus' method="POST"
                    enctype="multipart/form-data">@csrf
                    <h2>Apakah anda yakin ingin menghapus berkas?</h2>
                    <input type="hidden" name="id" id='id-hapus'>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                <button type="submit" form="form-hapus" class="btn btn-primary">Ya</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('script')
<script>
    var APP_URL = {!! json_encode(url('/')) !!}
    console.log(APP_URL)
    $('#keterangan-row').hide()
    $('#range-filter').daterangepicker()

    $('#add-berkas-btn').click(function(){
        $('#add-modal-title').text('Tambah Berkas')
        $('#add-modal').modal('show')
        $('#id-berkas').removeAttr('value')
        $('#nama').val('')
        $('#no-hp').val('')
        $('#jenis-berkas').val('').change()
        $('#form-berkas').attr('action',APP_URL+'/admin/berkas/file/upload')
        $('#file-peserta').attr('required',true)
    })

    $('body').on('click','.edit',function(){
        $('#add-modal-title').text('Edit Berkas')
        $('#add-modal').modal('show')
        $('#id-berkas').val($(this).data('id'))
        berkas = $(this).data('id')
        $('#form-berkas').attr('action',APP_URL+'/admin/berkas/'+berkas)
        $('#file-peserta').attr('required',false)
        $.ajax({
            url:APP_URL+'/admin/berkas/'+berkas,
            method:'GET',
            success:function(data){
                if (data.status==200){
                    $('#nama').val(data.berkas.nama)
                    $('#no-hp').val(data.berkas.no_hp)
                    $('#jenis-berkas').val(data.berkas.jenis_berkas).change()
                    $('#keterangan').text(data.berkas.keterangan)
                }
            }
        })
    })

    $('body').on('click','.delete',function(){
        console.log('aaa')
        $('#delete-modal').modal('show')
        $('#id-hapus').val($(this).data('id'))
    })

    $('#jenis-berkas').on('change', function(){
        let jenis = $(this).val()
        console.log(jenis)
        switch (jenis) {
            case "LAIN-LAIN":
                console.log('lain - lain')
                $('#keterangan-row').show()
                break
            default:
                $('#keterangan-row').hide()
                break
        }
    })
</script>
@endsection