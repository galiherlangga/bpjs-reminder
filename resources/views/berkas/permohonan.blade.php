<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Form Permohonan</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/dist/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <style>
        .register-page {
            background-image: linear-gradient(306deg, rgba(54, 54, 54, 0.05) 0%, rgba(54, 54, 54, 0.05) 33.333%, rgba(85, 85, 85, 0.05) 33.333%, rgba(85, 85, 85, 0.05) 66.666%, rgba(255, 255, 255, 0.05) 66.666%, rgba(255, 255, 255, 0.05) 99.999%), linear-gradient(353deg, rgba(81, 81, 81, 0.05) 0%, rgba(81, 81, 81, 0.05) 33.333%, rgba(238, 238, 238, 0.05) 33.333%, rgba(238, 238, 238, 0.05) 66.666%, rgba(32, 32, 32, 0.05) 66.666%, rgba(32, 32, 32, 0.05) 99.999%), linear-gradient(140deg, rgba(192, 192, 192, 0.05) 0%, rgba(192, 192, 192, 0.05) 33.333%, rgba(109, 109, 109, 0.05) 33.333%, rgba(109, 109, 109, 0.05) 66.666%, rgba(30, 30, 30, 0.05) 66.666%, rgba(30, 30, 30, 0.05) 99.999%), linear-gradient(189deg, rgba(77, 77, 77, 0.05) 0%, rgba(77, 77, 77, 0.05) 33.333%, rgba(55, 55, 55, 0.05) 33.333%, rgba(55, 55, 55, 0.05) 66.666%, rgba(145, 145, 145, 0.05) 66.666%, rgba(145, 145, 145, 0.05) 99.999%), linear-gradient(90deg, rgb(9, 201, 186), rgb(18, 131, 221));
        }
    </style>
</head>

<body class="hold-transition register-page">
    <div class="register-box">
        <div class="card">
            <div class="card-body register-card-body">
                <h2 class="login-box-msg">Formulir Permohonan</h2>
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <a href="#" class="close" id='close-alert' data-dismiss="alert">&times;</a>
                    <p>{{ $message }}</p>
                </div>
                @endif
                <form method="post" enctype="multipart/form-data" id='form-permohonan'>@csrf
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name='nama' placeholder="Nama" required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name='no_hp' placeholder="Nomor HP" required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-phone"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <select name="jenis_berkas" id="jenis-berkas" class="form-control" required>
                            <option value="">-- Pilih Jenis Berkas --</option>
                            <option value="BERKAS PENDAFTARAN PERUSAHAAN">BERKAS PENDAFTARAN PERUSAHAAN</option>
                            <option value="PERUBAHAN DATA TENAGA KERJA">PERUBAHAN DATA TENAGA KERJA</option>
                            <option value="AMALGAMASI (PENGGABUNGAN SALDO)">AMALGAMASI (PENGGABUNGAN SALDO)</option>
                            <option value="BERKAS PENUTUPAN PERUSAHAAN">BERKAS PENUTUPAN PERUSAHAAN</option>
                            <option value="LAIN-LAIN">LAIN-LAIN</option>
                        </select>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-file"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3" id='download-row'>
                        <div class="col-12">
                            <small class="text-muted" id='text-download'>Download berkas</small>
                        </div>
                        <div class="col-12">
                            <a href="" class="btn btn-primary" id='btn-download' target="_blank">Download</a>
                        </div>
                    </div>
                    <div class="input-group mb-3" id="keterangan-row">
                        <textarea name="keterangan" id="keterangan" rows="5" class="form-control"
                            placeholder="Keterangan"></textarea>
                    </div>
                    <div class="input-group mb-3">
                        <input type="file" name='file' class="form-control" placeholder="File"
                            accept="application/pdf, image/jpeg" required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-file"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-12">
                            <button type="submit" class="btn btn-success btn-block">Upload</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <div class="social-auth-links text-center mb-3">
                    <hr style="border:0.5px solid lightgray;">
                    <a href="{{ route('site.counter') }}" class="btn btn-primary" style='width:100%'>Pendaftaran Peserta
                        BPU</a>
                </div>
            </div>
            <!-- /.form-box -->
        </div><!-- /.card -->
    </div>
    <!-- /.register-box -->

    <!-- jQuery -->
    <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('assets/dist/js/adminlte.min.js') }}"></script>
    <script>
        var APP_URL = {!! json_encode(url('/')) !!}

        $('document').ready(function(){
            if ($('.card-body').find('.alert').length !== 0) {
                $('#form-permohonan').hide()
            }
        })

        $('#close-alert').click(function(){
            $('#form-permohonan').show()
        })

        $('#download-row').hide()
        $('#keterangan-row').hide()
        $('#jenis-berkas').on('change', function(){
            let jenis = $(this).val()
            switch (jenis) {
                case "BERKAS PENDAFTARAN PERUSAHAAN":
                    $('#download-row').show()
                    $('#keterangan-row').hide()
                    $('#text-download').text('Silahkan download formulir permohonan terlebih dahulu')
                    $('#btn-download').attr('href', APP_URL+'/storage/sample/form-permohonan.xls')
                    break
                case "PERUBAHAN DATA TENAGA KERJA":
                    $('#download-row').show()
                    $('#keterangan-row').hide()
                    $('#text-download').text('Silahkan download formulir perubahan data tenaga kerja terlebih dahulu')
                    $('#btn-download').attr('href', APP_URL+'/storage/sample/form-koreksi-tenaga-kerja.xlsx')
                    break
                case "AMALGAMASI (PENGGABUNGAN SALDO)":
                    $('#download-row').show()
                    $('#keterangan-row').hide()
                    $('#text-download').text('Silahkan download formulir amalgamasi terlebih dahulu')
                    $('#btn-download').attr('href', APP_URL+'/storage/sample/form-amalgamasi.pdf')
                    break
                case "LAIN-LAIN":
                    console.log('lainlain')
                    $('#download-row').hide()
                    $('#keterangan-row').show()
                    break
                default:
                    $('#download-row').hide()
                    $('#keterangan-row').hide()
                    break
            }
        })
    </script>
</body>

</html>