<!DOCTYPE html>
<html>

<head>
    @include('partial.header')
    @yield('style')
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar -->
        @include('partial.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('partial.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
            
        </div>
        <!-- /.content-wrapper -->

        @include('partial.footer')
    </div>
    <!-- ./wrapper -->
    @yield('modal')

    @include('partial.script')
    @yield('script')
</body>

</html>