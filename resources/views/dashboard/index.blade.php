@extends('base')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Dashboard</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <a href="#" class="close" id='close-alert' data-dismiss="alert">&times;</a>
            <p>{{ $message }}</p>
        </div>
        @endif
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title" id='grace-title'>Grace Tahun {{date('Y')}}</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-2">
                        <select id="filter-grace" class="form-control">
                            @for ($i = date('Y'); $i > 2000; $i--)
                            <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="chart">
                    <canvas id="barChart"
                        style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title" id='berkas-title'>Berkas Tahun 2021</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <form action="">
                    <div class="row mb-3">
                        <div class="col-2">
                            <select id="filter-berkas" name='year' class="form-control">
                                <option value="">Tahun</option>
                                @for ($i = date('Y'); $i > 2000; $i--)
                                <option value="{{$i}}" {{$filterYear==$i?'selected':''}}>{{$i}}</option>
                                @endfor
                            </select>
                            <input type="hidden" name="date" id='grace-to-berkas-date'>
                        </div>
                        <div class="col-2">
                            <select name="month" id="berkas-month-filter" class="form-control">
                                <option value="">Bulan</option>
                                @foreach ($month as $item)
                                <option value="{{$item}}" {{$filterMonth==$item?'selected':''}}>{{$item}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-2">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>
                </form>
                <div class="col-12">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama</th>
                                <th>No. HP</th>
                                <th>Jenis Berkas</th>
                                <th>Jumlah Hari</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($berkas as $key=>$item)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$item->nama}}</td>
                                <td>{{$item->no_hp}}</td>
                                <td>{{$item->jenis_berkas}}</td>
                                <td>{{$item->dayCount($item->created_at)}}</td>
                                <td>
                                    <a href="{{ asset('storage/berkas/'.$item->file) }}" target='_blank'
                                        class="btn btn-primary" title='lihat berkas'><i class="fa fa-eye"></i></a>
                                    <a href="{{ route('berkas.status', ['id'=>$item->id]) }}" class="btn btn-primary"
                                        title="ganti status"><i class="fa fa-sync"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title" id='visitor-title'>Visitor Tahun 2021</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-2">
                        <select id="filter-visitor" class="form-control">
                            @for ($i = date('Y'); $i > 2000; $i--)
                            <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="chart">
                    <canvas id="visitorChart"
                        style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        <!-- Default box -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Grace Hari Ini</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    {{-- <form action=""> --}}
                    <div class="col-3 mb-3">
                        <form id='form-filter-grace'>
                            <input type="date" name="date" id="filter-grace-table" class="form-control"
                                value="{{$filterDate}}">
                            <input type="hidden" name="year" id='berkas-to-grace-year'>
                            <input type="hidden" name="month" id='berkas-to-grace-month'>
                        </form>
                    </div>
                    <div class="col-2 mb-3">
                        <button type="submit" class="btn btn-primary" form="form-filter-grace">Filter</button>
                    </div>
                    {{-- </form> --}}
                    <div class="col-12">
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nomor</th>
                                    <th>Nama</th>
                                    <th>Tanggal Grace</th>
                                    <th>Program</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($peserta as $key=>$item)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$item->nomor}}</td>
                                    <td>{{$item->nama}}</td>
                                    <td>{{$item->tanggal_grace}}</td>
                                    <td>{{$item->program}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer ">
                <a href="#" class="btn btn-primary" data-toggle="modal" data-target='#import-modal'><i
                        class="fa fa-file-import"></i> Import File</a>
                <a href="{{ route('peserta.exportfile') }}" target="_blank" class="btn btn-primary float-right"><i
                        class="fa fa-file-export"></i> Export File</a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </div>


</section>
<!-- /.content -->
@endsection

@section('modal')
<div class="modal fade" id="import-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Import Data Peserta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('peserta.import') }}" id='form-import' method="POST"
                    enctype="multipart/form-data">@csrf
                    <div class="form-group">
                        <label for="file">File Peserta</label>
                        <input type="file" name="file" id="file-peserta" class="form-control"
                            accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                        <input type="hidden" name="callback" value="dashboard">
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" form="form-import" class="btn btn-primary">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('script')
<!-- ChartJS -->
<script src="{{ asset('assets/plugins/chart.js/Chart.min.js') }}"></script>
<script>
    var APP_URL = {!! json_encode(url('/')) !!}

    var dataMonthly = ajaxChart('/admin/getMonthly',$('#filter-grace').val())
    var dataCounter = ajaxChart('/admin/getCounter',2021)

    function ajaxChart(url, year) {
        let temp = null
        $.ajax({
            url:APP_URL+url,
            global:false,
            async:false,
            method:'GET',
            data: {
                year:year
            },
            success:function(data){
                temp = data
            }
        })
        return temp
    }

    function chartGenerate(id, data) {
        var areaChartData = {
          labels  : ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
          datasets: [
            {
              label               : 'Digital Goods',
              backgroundColor     : 'rgba(60,141,188,0.9)',
              borderColor         : 'rgba(60,141,188,0.8)',
              pointRadius          : false,
              pointColor          : '#3b8bba',
              pointStrokeColor    : 'rgba(60,141,188,1)',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(60,141,188,1)',
              data                : data
            },
          ]
        }
        //-------------
        //- BAR CHART -
        //-------------
        var barChartCanvas = $(id).get(0).getContext('2d')
        var barChartData = jQuery.extend(true, {}, areaChartData)
        var temp0 = areaChartData.datasets[0]
        // var temp1 = areaChartData.datasets[1]
        barChartData.datasets[0] = temp0
        // barChartData.datasets[1] = temp0

        var barChartOptions = {
            responsive              : true,
            maintainAspectRatio     : false,
            datasetFill             : false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        userCallback: function(label, index, labels) {
                            // when the floored value is the same as the value we have a whole number
                            if (Math.floor(label) === label) {
                                return label;
                            }
                        
                        },
                    }
                }],
            },
          
        }

        var barChart = new Chart(barChartCanvas, {
          type: 'bar', 
          data: barChartData,
          options: barChartOptions
        })
    }

    chartGenerate('#barChart', dataMonthly)
    chartGenerate('#visitorChart', dataCounter)

    $('#filter-grace').change(function(){
        console.log($(this).val())
        let year = $(this).val()
        let data = ajaxChart('/admin/getMonthly', year)
        chartGenerate('#barChart', data)
        $('#grace-title').text('Grace Tahun '+year)
    })

    $('#filter-visitor').change(function(){
        console.log($(this).val())
        let year = $(this).val()
        let data = ajaxChart('/admin/getCounter', year)
        chartGenerate('#visitorChart', data)
        $('#visitor-title').text('Visitor Tahun '+year)
    })
</script>
@endsection