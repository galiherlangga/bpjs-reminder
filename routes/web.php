<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::match(['GET','POST'], '/', 'BerkasController@permohonan')->name('berkas.permohonan');
Route::get('login', 'Auth\AuthController@index')->name('login');
Route::post('login', 'Auth\AuthController@login')->name('login');
Route::get('logout', 'Auth\AuthController@logout')->name('logout');

// counter
Route::get('/counter', 'SiteCounterController@counter')->name('site.counter');

Route::middleware(['auth'])->prefix('admin')->group(function() {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('/getMonthly', 'PesertaController@getMonthly')->name('peserta.getMonthly');
    Route::get('/getBerkas', 'BerkasController@getBerkas')->name('berkas.getBerkas');
    Route::get('/getCounter', 'SiteCounterController@getCounter')->name('counter.getCounter');
    Route::get('/status/{id}', 'BerkasController@changeStatus')->name('berkas.status');

    // peserta
    Route::resource('/peserta', 'PesertaController');
    Route::post('/peserta/import', 'PesertaController@import')->name('peserta.import');
    Route::get('/peserta/export', 'PesertaController@export')->name('peserta.exportfile');
    // berkas
    Route::get('/berkas', 'BerkasController@index')->name('berkas.index');
    Route::get('/berkas/{id}', 'BerkasController@edit')->name('berkas.edit');
    Route::post('/berkas/{id}','BerkasController@update')->name('berkas.update');
    Route::post('/berkas/file/upload', 'BerkasController@upload')->name('berkas.upload');
    Route::post('/berkas/deletefile', 'BerkasController@deletefile')->name('berkas.deletefile');
});